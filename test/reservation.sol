// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.8.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Reservation.sol";

contract TestReservation {
    // The address of the reservation contract to be tested
    Reservation reservation = Reservation(DeployedAddresses.Reservation());

    // The id of the seat that will be used for testing
    uint256 expectedSeatId = 8;

    //The expected owner of seat is this contract
    address expectedOwner = address(this);

    // Testing the book() function
    function testUserCanBookSeat() public {
        uint256 returnedId = reservation.book(expectedSeatId);

        Assert.equal(
            returnedId,
            expectedSeatId,
            "Reservation of the expected seat should match what is returned."
        );
    }

    // Testing retrieval of a single seat's owner
    function testGetCustomerAddressBySeatId() public {
        address customer = reservation.seats(expectedSeatId);

        Assert.equal(
            customer,
            expectedOwner,
            "Owner of the expected seat should be this contract"
        );
    }

    // Testing retrieval of all seat owners
    function testGetCustomerAddressBySeatIdInArray() public {
        // Store customers in memory rather than contract's storage
        address[24] memory customers = reservation.reservations();

        Assert.equal(
            customers[expectedSeatId],
            expectedOwner,
            "Owner of the expected pet should be this contract"
        );
    }
}
