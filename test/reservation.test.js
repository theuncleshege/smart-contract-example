const Reservation = artifacts.require('Reservation');
const truffleAsserts = require('truffle-assertions');

contract('Reservation', (accounts) => {
  let reservation;
  let expectedCustomer;

  before(async () => {
    reservation = await Reservation.deployed();
  });

  describe('booking a seat and retrieving account addresses', async () => {
    before('book a seat using accounts[0]', async () => {
      await reservation.book(8, { from: accounts[0] });
      expectedCustomer = accounts[0];
    });

    it('can fetch the address of a customer by seat number', async () => {
      const customer = await reservation.seats(8);
      assert.equal(
        customer,
        expectedCustomer,
        'The owner of the seat should be the first account.'
      );
    });

    it("can fetch the reservations of owners' addresses", async () => {
      const reservations = await reservation.reservations();
      assert.equal(
        reservations[8],
        expectedCustomer,
        'The owner of the seat should be in the reservations.'
      );
    });

    it('fails to reserve a seat if the seat is already booked', async () => {
      await truffleAsserts.reverts(
        reservation.book(8, { from: accounts[1] }),
        'Seat unavailable'
      );
    });
  });
});
