// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.8.0;

contract Reservation {
	address[24] public seats;

	event Booked(address owner, uint seatNumber);

	function book(uint256 seatId) public returns (uint256) {
		require(seatId >= 0 && seatId <= 23, "Seat unavailable");

		require(seats[seatId] == address(0), "Seat unavailable");

		seats[seatId] = msg.sender;

		emit Booked(msg.sender, seatId);

		return seatId;
	}

	function reservations() public view returns (address[24] memory) {
		return seats;
	}
}
